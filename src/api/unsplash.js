import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization:
            'Client-ID gzqNhZJwHUTX7oiV0SBTFoRA6lh2Op3uckbQf1I_cSw'
    }
});
